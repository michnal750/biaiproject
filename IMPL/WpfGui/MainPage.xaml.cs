﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace BiaiProject
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : UserControl
    {
        private Uri imageUri;
        private Thread thread;

        public MainPage()
        {
            InitializeComponent();
        }

        private void LoadPhotoButton_Click(object sender, RoutedEventArgs e)
        {
            resultText.Text = "Result";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a picture";
            openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png;*.bmp|" +
              "JPEG |*.jpg;*.jpeg|" +
              "Bitmap Image | *.bmp|" +
              "Portable Network Graphic |*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                NoFileLoadedText.Text = "";
                imageUri = new Uri(openFileDialog.FileName);
                MainImage.Source = new BitmapImage(imageUri);
            }
        }

        private void RecogniseButton_Click(object sender, RoutedEventArgs e)
        {
            if (imageUri != null)
            {
                if (thread == null || !thread.IsAlive)
                {
                    thread = new Thread(new ThreadStart(RecogniseEmotion));
                    thread.Start();
                    loadingSpinner.Visibility = Visibility.Visible;
                }
            }
        }

        private void RecogniseEmotion()
        {
            var imagePath = imageUri.OriginalString;
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:\Users\Michal PC\AppData\Local\Programs\Python\Python37\python.exe"; // link do pythona
            var script = @"D:\biaiproject\pythonCNN\test.py"; // link do naszego skryptu
            psi.Arguments = $"\"{script}\" \"{imagePath}\"";
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.CreateNoWindow = true;
            string result = "";
            string error = "";
            using (var process = Process.Start(psi))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    error = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                    result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                }
                File.WriteAllText(@"C:\Users\Michal PC\Desktop\errors.txt", error);
                Dispatcher.Invoke(() =>
                {
                    resultText.Text = result;
                    loadingSpinner.Visibility = Visibility.Hidden;
                });
            }
        }
    }
}
