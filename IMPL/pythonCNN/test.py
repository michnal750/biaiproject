#!/usr/bin/python
import tensorflow as tf
import keras
import numpy as np
from PIL import Image
from resizeimage import resizeimage
import sys

path = sys.argv[1]
CATEGORIES = ["Anger", "Happy", "Neutral", "Surprise", "Sad"]
imgHeight = 48
imgWidth = 48
#path = 'D:\\biaiproject\\pythonCNN\\anger.png'
with open(path, 'rb') as f:
    with Image.open(f) as image:
        cover = resizeimage.resize_cover(image, [48, 48])
        cover.save('cover.png', image.format)
       
cover2 = Image.open('cover.png').convert('LA')
cover2.save('greyscale.png')

model = tf.keras.models.load_model("CNNmodel4.h5")

image = tf.keras.preprocessing.image.load_img('greyscale.png')
input_arr = keras.preprocessing.image.img_to_array(image)
input_arr = np.array([input_arr])  # Convert single image to a batch.
prediction = (model.predict([input_arr])).astype("float32")
#print(prediction)
index = np.argmax(prediction[0])
print(CATEGORIES[index])